--Commenting
CREATE TABLE vn04lp7.mu_dept_lpi_dc_base_8mar stored AS ORC AS 
SELECT 
			ugc_id,
			customer_id,
			group_order_nbr,
			auth_date,
			catalog_item_id,
			service_id,
			SUM(total_auth_amount) AS sales,
			SUM(total_auth_quantity) AS units 
FROM 
			gcia_dotcom.gcia_ugc_sol_order_360_no_exc_ch7_v7
WHERE 
			service_id IN (0,4,7,8,10,11,12,14,18,22)
			AND auth_date BETWEEN '2015-02-01' AND '2018-01-31'
			AND flag_blank_ugcid=0 
			AND flag_blank_ctlg_item_id=1 
GROUP BY 
			ugc_id,
			customer_id,
			group_order_nbr,
			auth_date,
			catalog_item_id,
			service_id;

------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE vn04lp7.mu_dept_kpi_dc_w_desc_13mar STORED AS ORC AS
SELECT *,
			CASE WHEN auth_date BETWEEN '2016-02-01' AND '2016-04-30' THEN 'Q1'
						WHEN auth_date BETWEEN '2016-05-01' AND '2016-07-31' THEN 'Q2'
						WHEN auth_date BETWEEN '2016-08-01' ANd '2016-10-31' THEN 'Q3'
						WHEN auth_date BETWEEN '2016-11-01' AND '2017-01-31' THEN 'Q4'
						WHEN auth_date BETWEEN '2017-02-01' AND '2017-04-30' THEN 'Q1'
						WHEN auth_date BETWEEN '2017-05-01' AND '2017-07-31' THEN 'Q2'
						WHEN auth_date BETWEEN '2017-08-01' AND '2017-10-31' THEN 'Q3'
						WHEN auth_date BETWEEN '2017-11-01' AND '2018-01-31' THEN 'Q4' 
			END AS fiscal_qtr_name,

			CASE WHEN auth_date BETWEEN '2016-02-01' AND '2017-01-31' THEN 2017
						WHEN auth_date BETWEEN '2017-02-01' AND '2018-01-31' THEN 2018 
			END AS fiscal_year_nbr
FROM
			(
					SELECT 
								base .*, 
								super_department_id,
								super_Dept,
								department_id,
								dept,
								category_id,
								cat,sub_category_id,
								subcat
					FROM vn04lp7.mu_dept_lpi_dc_base_8mar base
					INNER JOIN
					(
							SELECT 
										catalog_item_id,
										super_department_id,
										super_Dept,
										department_id,
										dept,
										category_id,
										cat,
										sub_category_id,
										subcat 
							FROM 
										gcia_dotcom.uber_item_info_single_seller_replica 
							GROUP BY 
										catalog_item_id,
										super_department_id,
										super_Dept,
										department_id,
										dept,
										category_id,
										cat,
										sub_category_id,
										subcat 
					)  b
				ON base.catalog_item_id=b.catalog_item_id
			) c

------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE vn04lp7.mu_dept_kpi_dc_base_table_pos_final_13mar STORED AS ORC AS
SELECT 
			base.*, 
			store_acctg_dept_nbr,
			store_dept_category_nbr,
			store_dept_subcatg_nbr
FROM 
			vn04lp7.mu_dept_kpi_dc_w_desc_13mar base
INNER JOIN
			vn0f7e5.mu_instore_dotcom_mapping_table_0301 mapp
			ON base.super_department_id=mapp.dotcom_super_dept_nbr
			AND base.department_id=mapp.dotcom_dept_nbr
			AND base.sub_category_id=mapp.dotcom_dept_subcatg_nbr
			AND base.category_id=mapp.dotcom_category_nbr

